package com.coreyeckelman.hadoop.module3;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

public class StockVolumeTest {
	protected MapReduceDriver<LongWritable, Text, Text, LongWritable, Text, LongWritable> mapReduceDriver;
	protected MapDriver<LongWritable, Text, Text, LongWritable> mapDriver;
	protected ReduceDriver<Text, LongWritable, Text, LongWritable> reduceDriver;
	
	@Before
	public void setUp() {
		//Create each of the drivers, which mrunit uses to unit test the mapreduce job.
		StockVolumeMapper mapper = new StockVolumeMapper();
		mapDriver = new MapDriver<>();
		mapDriver.setMapper(mapper);
		
		SumReducer reducer = new SumReducer();
		reduceDriver = new ReduceDriver<>();
		reduceDriver.setReducer(reducer);
		
		mapReduceDriver = new MapReduceDriver<>();
		mapReduceDriver.setMapper(mapper);
		mapReduceDriver.setReducer(reducer);
	}
	
	@Test
	public void testMapper() throws Exception {
		//Runs a test map() and ensures that the record is transformed correctly.
		//Expects the given record to return (ABXA, 158500)
		mapDriver
			.withInput(new LongWritable(0), new Text("NASDAQ,ABXA,12/9/2009,2.55,2.77,2.5,2.67,158500,2.67"))
			.withOutput(new Text("ABXA"), new LongWritable(158500))
			.runTest();
	}
	
	@Test
	public void testReducer() throws Exception {
		//Runs a test reduce() and ensures that each of the records is reduced correctly.
		//Expects the given records to return ("TEST", 6)
		List<LongWritable> values = new ArrayList<LongWritable>();
		values.add(new LongWritable(1));
		values.add(new LongWritable(2));
		values.add(new LongWritable(3));
		
		reduceDriver
			.withInput(new Text("TEST"), values)
			.withOutput(new Text("TEST"), new LongWritable(6))
			.runTest();
	}
	
	@Test
	public void testMapReduce() throws Exception {
		//Runs a test map and feeds it to a test reduce, then ensures the values are expected.
		//Expects the given records to return ("ABXA", 17).
		
		mapReduceDriver
			.withInput(new LongWritable(0), new Text("NASDAQ,ABXA,12/9/2009,2.55,2.77,2.5,2.67,2,2.67"))
			.withInput(new LongWritable(1), new Text("NASDAQ,ABXA,12/9/2009,2.55,2.77,2.5,2.67,3,2.67"))
			.withInput(new LongWritable(2), new Text("NASDAQ,ABXA,12/9/2009,2.55,2.77,2.5,2.67,5,2.67"))
			.withInput(new LongWritable(3), new Text("NASDAQ,ABXA,12/9/2009,2.55,2.77,2.5,2.67,7,2.67"))
			.withOutput(new Text("ABXA"), new LongWritable(17))
			.runTest();
	
	}
}
