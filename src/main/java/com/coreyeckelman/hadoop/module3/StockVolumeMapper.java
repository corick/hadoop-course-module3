package com.coreyeckelman.hadoop.module3;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Maps record -> (record.stock_name, record.volume).
 */
public class StockVolumeMapper
	extends Mapper<LongWritable, Text, Text, LongWritable> {
	
	private Text outText = new Text();
	private LongWritable outVolume = new LongWritable();
	
	@Override 
	protected void map(LongWritable key, Text value, Context context) 
			throws IOException,
			       InterruptedException {
		
		//Split the csv file by ','
		StringTokenizer tokenizer = new StringTokenizer(value.toString(), ",");
			
		//And skip the header record.
		if (tokenizer.nextToken().equals("exchange")) //FIXME: Can we just check if key == 0?
			return;
		
		//Get the stock ticker name.
		String stockName = tokenizer.nextToken();
		
		//skip the next few records then
		for(int i = 0; i < 5; i++)
			tokenizer.nextToken();
		
		//grab the volume as well
		String stockVolume = tokenizer.nextToken();
		
		long longVolume;
		try {
            longVolume = Long.parseLong(stockVolume);
		}
		catch(NumberFormatException e) {
			return;
		}
		
		//Emit this key/value pair.
		outText.set(stockName);
		outVolume.set(longVolume);
		context.write(outText, outVolume);
	};
}
