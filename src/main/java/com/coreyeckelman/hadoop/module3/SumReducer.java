package com.coreyeckelman.hadoop.module3;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Reduces (Text key, Long value) by summing each of the values.
 */
public class SumReducer 
	extends Reducer<Text, LongWritable, Text, LongWritable>{
	
	private LongWritable volumeWritable = new LongWritable();
	
	@Override
	protected void reduce(Text key, Iterable<LongWritable> values, Context context)
			throws IOException, InterruptedException {
		long volume = 0;
		
		for (LongWritable value : values) {
			volume += value.get();
		}
		
		volumeWritable.set(volume);
		context.write(key, volumeWritable);
	}
}
