package com.coreyeckelman.hadoop.module3;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Hadoop launcher for assignment3 things.
 */
public class App 
{
    public static void main( String[] args ) 
    		throws Exception
    {
    	//Create the job.
    	Configuration conf = new Configuration();
    	Job currentJob = Job.getInstance(conf, "module3");
    	
    	currentJob.setJarByClass(App.class);
    	
    	String assignment = args[0];
    	switch(Integer.parseInt(assignment))
    	{
    	case 1: //Set up map/reduce for the first assignment. 
    		currentJob.setMapperClass(StockVolumeMapper.class);
    		currentJob.setReducerClass(SumReducer.class);
    		currentJob.setJobName("module-3-assignment-1");
    		currentJob.setMapOutputKeyClass(Text.class);
    		currentJob.setMapOutputValueClass(LongWritable.class);
    		currentJob.setOutputKeyClass(Text.class);
    		currentJob.setOutputValueClass(LongWritable.class);
    		break;
    		
    	default: throw new Exception("not supported!");
    	}
    	
    	//set the input and output based on the jar's arguments.
    	FileInputFormat.addInputPath(currentJob, new Path(args[1]));
        FileOutputFormat.setOutputPath(currentJob, new Path(args[2]));
        
        //run the job.
        currentJob.waitForCompletion(true);
    }
}
